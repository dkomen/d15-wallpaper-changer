## Introduction

**To receive a free registration key send a request email to: d15walls@dimension15.co.za**

A quick coding of a Windows 8/10/11 desktop wallpaper changer with 2000+ images of various screen ratios and resolutions from the Dimension15 website. 

Retrieves wallpapers that are an exact or near fit to your Windows primary screens' width and height. It can also change the desktop wallpaper in un-activated Windows installations.

If you did not install this program with the D15Walls_Setup.exe file then in order to run the executable when Windows starts/restarts just add a shortcut to the executable (D15Walls.exe) in your Startup folder (Open shell:Startup in Windows Explorer to view your Startup folder).

**The .Net Core 3.1 framework must be installed on your Windows computer in order to run this application.**
