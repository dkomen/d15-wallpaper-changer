﻿
namespace D15Walls
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.chkLocalOnly = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rdbRandom1to72 = new System.Windows.Forms.RadioButton();
            this.rdbUpto2Weeks = new System.Windows.Forms.RadioButton();
            this.rdbFirstDayOfMonth = new System.Windows.Forms.RadioButton();
            this.rdbHour = new System.Windows.Forms.RadioButton();
            this.rdb8Hours = new System.Windows.Forms.RadioButton();
            this.rdbMondayMornings = new System.Windows.Forms.RadioButton();
            this.rdb48Hours = new System.Windows.Forms.RadioButton();
            this.rdbMorning = new System.Windows.Forms.RadioButton();
            this.chkScreenResolution = new System.Windows.Forms.CheckBox();
            this.btnRestartTimer = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblTimerDisplay = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnSaveCurrent = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.btnPrevious = new System.Windows.Forms.Button();
            this.chkAlwaysSaveImages = new System.Windows.Forms.CheckBox();
            this.toolTipSaveCurrent = new System.Windows.Forms.ToolTip(this.components);
            this.btnPause = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // chkLocalOnly
            // 
            this.chkLocalOnly.AutoSize = true;
            this.chkLocalOnly.Location = new System.Drawing.Point(12, 312);
            this.chkLocalOnly.Name = "chkLocalOnly";
            this.chkLocalOnly.Size = new System.Drawing.Size(193, 19);
            this.chkLocalOnly.TabIndex = 11;
            this.chkLocalOnly.Text = "Only use images in \"Favourites\"";
            this.toolTipSaveCurrent.SetToolTip(this.chkLocalOnly, "Do not download from the web, only use saved favourites");
            this.chkLocalOnly.UseVisualStyleBackColor = true;
            this.chkLocalOnly.CheckedChanged += new System.EventHandler(this.chkLocalOnly_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rdbRandom1to72);
            this.groupBox2.Controls.Add(this.rdbUpto2Weeks);
            this.groupBox2.Controls.Add(this.rdbFirstDayOfMonth);
            this.groupBox2.Controls.Add(this.rdbHour);
            this.groupBox2.Controls.Add(this.rdb8Hours);
            this.groupBox2.Controls.Add(this.rdbMondayMornings);
            this.groupBox2.Controls.Add(this.rdb48Hours);
            this.groupBox2.Controls.Add(this.rdbMorning);
            this.groupBox2.Location = new System.Drawing.Point(12, 81);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(287, 154);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Update frequency";
            // 
            // rdbRandom1to72
            // 
            this.rdbRandom1to72.AutoSize = true;
            this.rdbRandom1to72.Location = new System.Drawing.Point(13, 125);
            this.rdbRandom1to72.Name = "rdbRandom1to72";
            this.rdbRandom1to72.Size = new System.Drawing.Size(186, 19);
            this.rdbRandom1to72.TabIndex = 13;
            this.rdbRandom1to72.Text = "Randomly (1 hour to 72 hours)";
            this.toolTipSaveCurrent.SetToolTip(this.rdbRandom1to72, "Randomly between 1 hour and 3 days");
            this.rdbRandom1to72.UseVisualStyleBackColor = true;
            this.rdbRandom1to72.CheckedChanged += new System.EventHandler(this.rdbRandom1to72_CheckedChanged);
            // 
            // rdbUpto2Weeks
            // 
            this.rdbUpto2Weeks.AutoSize = true;
            this.rdbUpto2Weeks.Location = new System.Drawing.Point(13, 100);
            this.rdbUpto2Weeks.Name = "rdbUpto2Weeks";
            this.rdbUpto2Weeks.Size = new System.Drawing.Size(182, 19);
            this.rdbUpto2Weeks.TabIndex = 14;
            this.rdbUpto2Weeks.Text = "Randomly (1 hour to 2 weeks)";
            this.rdbUpto2Weeks.UseVisualStyleBackColor = true;
            this.rdbUpto2Weeks.CheckedChanged += new System.EventHandler(this.rdbUpto2Weeks_CheckedChanged);
            // 
            // rdbFirstDayOfMonth
            // 
            this.rdbFirstDayOfMonth.AutoSize = true;
            this.rdbFirstDayOfMonth.Location = new System.Drawing.Point(130, 75);
            this.rdbFirstDayOfMonth.Name = "rdbFirstDayOfMonth";
            this.rdbFirstDayOfMonth.Size = new System.Drawing.Size(146, 19);
            this.rdbFirstDayOfMonth.TabIndex = 12;
            this.rdbFirstDayOfMonth.Text = "Every 1st day of month";
            this.toolTipSaveCurrent.SetToolTip(this.rdbFirstDayOfMonth, "Every first day of the month at 07:00");
            this.rdbFirstDayOfMonth.UseVisualStyleBackColor = true;
            this.rdbFirstDayOfMonth.CheckedChanged += new System.EventHandler(this.rdbFirstDayOfMonth_CheckedChanged);
            // 
            // rdbHour
            // 
            this.rdbHour.AutoSize = true;
            this.rdbHour.Location = new System.Drawing.Point(13, 25);
            this.rdbHour.Name = "rdbHour";
            this.rdbHour.Size = new System.Drawing.Size(81, 19);
            this.rdbHour.TabIndex = 3;
            this.rdbHour.Text = "Every hour";
            this.rdbHour.UseVisualStyleBackColor = true;
            this.rdbHour.CheckedChanged += new System.EventHandler(this.rdbHour_CheckedChanged);
            // 
            // rdb8Hours
            // 
            this.rdb8Hours.AutoSize = true;
            this.rdb8Hours.Location = new System.Drawing.Point(13, 50);
            this.rdb8Hours.Name = "rdb8Hours";
            this.rdb8Hours.Size = new System.Drawing.Size(95, 19);
            this.rdb8Hours.TabIndex = 9;
            this.rdb8Hours.Text = "Every 8 hours";
            this.rdb8Hours.UseVisualStyleBackColor = true;
            this.rdb8Hours.CheckedChanged += new System.EventHandler(this.rdb8Hours_CheckedChanged);
            // 
            // rdbMondayMornings
            // 
            this.rdbMondayMornings.AutoSize = true;
            this.rdbMondayMornings.Location = new System.Drawing.Point(130, 50);
            this.rdbMondayMornings.Name = "rdbMondayMornings";
            this.rdbMondayMornings.Size = new System.Drawing.Size(123, 19);
            this.rdbMondayMornings.TabIndex = 11;
            this.rdbMondayMornings.Text = "Monday mornings";
            this.toolTipSaveCurrent.SetToolTip(this.rdbMondayMornings, "Every Monday morning at 07:00");
            this.rdbMondayMornings.UseVisualStyleBackColor = true;
            this.rdbMondayMornings.CheckedChanged += new System.EventHandler(this.rdbMondayMornings_CheckedChanged);
            // 
            // rdb48Hours
            // 
            this.rdb48Hours.AutoSize = true;
            this.rdb48Hours.Location = new System.Drawing.Point(13, 75);
            this.rdb48Hours.Name = "rdb48Hours";
            this.rdb48Hours.Size = new System.Drawing.Size(101, 19);
            this.rdb48Hours.TabIndex = 15;
            this.rdb48Hours.Text = "Every 48 hours";
            this.rdb48Hours.UseVisualStyleBackColor = true;
            this.rdb48Hours.CheckedChanged += new System.EventHandler(this.rdb48Hours_CheckedChanged);
            // 
            // rdbMorning
            // 
            this.rdbMorning.AutoSize = true;
            this.rdbMorning.Checked = true;
            this.rdbMorning.Location = new System.Drawing.Point(130, 25);
            this.rdbMorning.Name = "rdbMorning";
            this.rdbMorning.Size = new System.Drawing.Size(102, 19);
            this.rdbMorning.TabIndex = 10;
            this.rdbMorning.TabStop = true;
            this.rdbMorning.Text = "Every morning";
            this.toolTipSaveCurrent.SetToolTip(this.rdbMorning, "Every morning at 07:00");
            this.rdbMorning.UseVisualStyleBackColor = true;
            this.rdbMorning.CheckedChanged += new System.EventHandler(this.rdbMorning_CheckedChanged);
            // 
            // chkScreenResolution
            // 
            this.chkScreenResolution.AutoSize = true;
            this.chkScreenResolution.Location = new System.Drawing.Point(12, 362);
            this.chkScreenResolution.Name = "chkScreenResolution";
            this.chkScreenResolution.Size = new System.Drawing.Size(276, 19);
            this.chkScreenResolution.TabIndex = 13;
            this.chkScreenResolution.Text = "Try retrieve images for current screen resolution";
            this.chkScreenResolution.UseVisualStyleBackColor = true;
            this.chkScreenResolution.CheckedChanged += new System.EventHandler(this.chkScreenResolution_CheckedChanged);
            // 
            // btnRestartTimer
            // 
            this.btnRestartTimer.Location = new System.Drawing.Point(184, 24);
            this.btnRestartTimer.Name = "btnRestartTimer";
            this.btnRestartTimer.Size = new System.Drawing.Size(89, 23);
            this.btnRestartTimer.TabIndex = 0;
            this.btnRestartTimer.Text = "Restart timer";
            this.btnRestartTimer.UseVisualStyleBackColor = true;
            this.btnRestartTimer.Click += new System.EventHandler(this.btnRestartTimer_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblTimerDisplay);
            this.groupBox1.Controls.Add(this.btnRestartTimer);
            this.groupBox1.Location = new System.Drawing.Point(12, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(287, 62);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Refresh timer";
            // 
            // lblTimerDisplay
            // 
            this.lblTimerDisplay.AutoSize = true;
            this.lblTimerDisplay.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblTimerDisplay.ForeColor = System.Drawing.Color.Blue;
            this.lblTimerDisplay.Location = new System.Drawing.Point(8, 19);
            this.lblTimerDisplay.Name = "lblTimerDisplay";
            this.lblTimerDisplay.Size = new System.Drawing.Size(65, 28);
            this.lblTimerDisplay.TabIndex = 10;
            this.lblTimerDisplay.Text = "label1";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnSaveCurrent);
            this.groupBox3.Controls.Add(this.btnNext);
            this.groupBox3.Controls.Add(this.btnPrevious);
            this.groupBox3.Location = new System.Drawing.Point(12, 241);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(284, 65);
            this.groupBox3.TabIndex = 11;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Refresh now";
            // 
            // btnSaveCurrent
            // 
            this.btnSaveCurrent.Location = new System.Drawing.Point(184, 23);
            this.btnSaveCurrent.Name = "btnSaveCurrent";
            this.btnSaveCurrent.Size = new System.Drawing.Size(87, 30);
            this.btnSaveCurrent.TabIndex = 10;
            this.btnSaveCurrent.Text = "Save current";
            this.toolTipSaveCurrent.SetToolTip(this.btnSaveCurrent, "Save current wallpaper to favourites");
            this.btnSaveCurrent.UseVisualStyleBackColor = true;
            this.btnSaveCurrent.Click += new System.EventHandler(this.btnSaveCurrent_Click);
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(99, 23);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(69, 30);
            this.btnNext.TabIndex = 9;
            this.btnNext.Text = "Next";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnPrevious
            // 
            this.btnPrevious.Location = new System.Drawing.Point(13, 23);
            this.btnPrevious.Name = "btnPrevious";
            this.btnPrevious.Size = new System.Drawing.Size(72, 30);
            this.btnPrevious.TabIndex = 8;
            this.btnPrevious.Text = "Previous";
            this.btnPrevious.UseVisualStyleBackColor = true;
            this.btnPrevious.Click += new System.EventHandler(this.btnPrevious_Click);
            // 
            // chkAlwaysSaveImages
            // 
            this.chkAlwaysSaveImages.AutoSize = true;
            this.chkAlwaysSaveImages.Location = new System.Drawing.Point(12, 337);
            this.chkAlwaysSaveImages.Name = "chkAlwaysSaveImages";
            this.chkAlwaysSaveImages.Size = new System.Drawing.Size(168, 19);
            this.chkAlwaysSaveImages.TabIndex = 12;
            this.chkAlwaysSaveImages.Text = "Always save images to disk";
            this.toolTipSaveCurrent.SetToolTip(this.chkAlwaysSaveImages, "All web images will be saved as favourites");
            this.chkAlwaysSaveImages.UseVisualStyleBackColor = true;
            this.chkAlwaysSaveImages.CheckedChanged += new System.EventHandler(this.chkAlwaysSaveImages_CheckedChanged);
            // 
            // btnPause
            // 
            this.btnPause.Location = new System.Drawing.Point(12, 387);
            this.btnPause.Name = "btnPause";
            this.btnPause.Size = new System.Drawing.Size(85, 30);
            this.btnPause.TabIndex = 14;
            this.btnPause.Text = "&Pause...";
            this.toolTipSaveCurrent.SetToolTip(this.btnPause, "Pause refreshing of the desktop wallpaper");
            this.btnPause.UseVisualStyleBackColor = true;
            this.btnPause.Click += new System.EventHandler(this.btnPause_Click);
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(220, 387);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(76, 30);
            this.btnExit.TabIndex = 15;
            this.btnExit.Text = "E&xit";
            this.toolTipSaveCurrent.SetToolTip(this.btnExit, "Exit the program");
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(312, 424);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnPause);
            this.Controls.Add(this.chkAlwaysSaveImages);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.chkScreenResolution);
            this.Controls.Add(this.chkLocalOnly);
            this.Controls.Add(this.groupBox2);
            this.Name = "Form1";
            this.Text = "D15 Wallpapers V1.2";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.CheckBox chkLocalOnly;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rdbHour;
        private System.Windows.Forms.CheckBox chkScreenResolution;
        private System.Windows.Forms.Button btnRestartTimer;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblTimerDisplay;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button btnPrevious;
        private System.Windows.Forms.CheckBox chkAlwaysSaveImages;
        private System.Windows.Forms.Button btnSaveCurrent;
        private System.Windows.Forms.ToolTip toolTipSaveCurrent;
        private System.Windows.Forms.RadioButton rdb8Hours;
        private System.Windows.Forms.RadioButton rdbMorning;
        private System.Windows.Forms.RadioButton rdbFirstDayOfMonth;
        private System.Windows.Forms.RadioButton rdbMondayMornings;
        private System.Windows.Forms.RadioButton rdbRandom1to72;
        private System.Windows.Forms.RadioButton rdbUpto2Weeks;
        private System.Windows.Forms.RadioButton rdb48Hours;
        private System.Windows.Forms.Button btnPause;
        private System.Windows.Forms.Button btnExit;
    }
}

