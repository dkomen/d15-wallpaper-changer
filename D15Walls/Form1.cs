﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Net;
using System.Reflection;
using System.Windows.Forms;

namespace D15Walls
{
    public partial class Form1 : Form
    {
        #region Initialisers
        private const int ID_MORNINGS = 1000000;
        private const int ID_MONDAY_MORNINGS = 2000000;
        private const int ID_FIRST_OF_MONTH = 3000000;
        private const int ID_RANDOM_1_TO_72 = 4000000;
        private const int ID_RANDOM_1_TO_2_WEEKS = 5000000;

        private const string MESSAGE_BOX_TITLE = "D15 Wallpapers";
        private const int RANDOM_MIN = 60;
        private const int RANDOM_MAX_72_HOURS = 72 * 60;
        private const int RANDOM_MAX_2_WEEKS = 14 * 24 * 60;
        private string PATH_TO_SETTINGS = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyPictures), "D15Walls");
        private string PATH_TO_WORKING_IMAGES = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyPictures), "D15Walls", ".working_images");
        private string PATH_TO_FAVOURITE_IMAGES = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyPictures), "D15Walls", "Favourites");
        private string CURRENT_IMAGE_NAME = string.Empty;
        private System.Timers.Timer countdownTimer = new System.Timers.Timer(1000);
        private List<string> history = new List<string>();
        #endregion

        #region Fields
        private Icon GoodIcon { get; set; }
        private Icon BadIcon { get; set; }
        private static NotifyIcon trayIcon;
        private static ContextMenuStrip trayMenu;                
        private bool FormIsLoading { get; set; }        
        private int CurrentHistoryIndex { get; set; }
        private bool UseHistory { get; set; }
        private bool Exit { get; set; }        
        private Settings Settings { get; set; }                
        private bool RetrieveForCurrentScreenResolution { get; set; }
        #endregion

        public Form1()
        {
            InitializeComponent();

            if (System.IO.Directory.Exists(PATH_TO_WORKING_IMAGES)) 
            {
                System.IO.Directory.Delete(PATH_TO_WORKING_IMAGES, true);
            }

            this.FormIsLoading = true;
            this.ShowInTaskbar = false;
            this.StartPosition = FormStartPosition.CenterScreen;
            this.Opacity = 0;
            this.FormBorderStyle = FormBorderStyle.FixedToolWindow;
            this.FormClosing += this.Form1_FormClosing;

            //Settings = new Settings(System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), MESSAGE_BOX_TITLE, ID_MORNINGS);
            Settings = new Settings(PATH_TO_SETTINGS, MESSAGE_BOX_TITLE, ID_MORNINGS);
            if (String.IsNullOrEmpty(Settings.RegistrationKey))
            {
                MessageBox.Show("You are not a registered user", MESSAGE_BOX_TITLE);
                Application.Exit();
            }

            #region Create the system tray control
            trayMenu = new ContextMenuStrip();
            trayMenu.Items.Add("Next", null, OnNextWallpaper);
            trayMenu.Items.Add("Previous", null, OnPreviousWallpaper);
            trayMenu.Items.Add("Save as favourite", null, OnSveCurrentToDisk);
            trayMenu.Items.Add("-");
            trayMenu.Items.Add("Pause...", null, OnPause);
            trayMenu.Items.Add("-");
            trayMenu.Items.Add("Settings", null, OnSettings);
            trayMenu.Items.Add("-");
            trayMenu.Items.Add("Exit", null, OnExit);

            trayIcon = new NotifyIcon();
            trayIcon.Text = MESSAGE_BOX_TITLE;

            Properties.Resources.RGBIcon.MakeTransparent(Color.White);
            var iconHandle = Properties.Resources.RGBIcon.GetHicon();
            Icon d15Icon = Icon.FromHandle(iconHandle);
            this.GoodIcon = d15Icon;

            Properties.Resources.RGBBadIcon.MakeTransparent(Color.White);
            var badIconHandle = Properties.Resources.RGBBadIcon.GetHicon();
            Icon badD15Icon = Icon.FromHandle(badIconHandle);
            this.BadIcon = badD15Icon;

            trayIcon.Icon = this.GoodIcon;

            trayIcon.ContextMenuStrip = trayMenu;
            trayIcon.Visible = true;
            #endregion

            UseHistory = false;
            Settings.IsPaused = false;
        }


        /// <summary>
        /// Triggered when the walpaper must be changed
        /// </summary>
        private void CycleWallpaper()
        {
            if (Settings.IsPaused) { return; }

            string imagePath = string.Empty;

            if (UseHistory)
            {
                if (CurrentHistoryIndex >= 0)
                {
                    imagePath = history[CurrentHistoryIndex];
                    string fileName = System.IO.Path.GetFileName(imagePath);
                    if (!System.IO.File.Exists(System.IO.Path.Combine(PATH_TO_WORKING_IMAGES, fileName)))
                    {
                        MessageBox.Show("History image not found in cache", MESSAGE_BOX_TITLE);
                    }
                    else
                    {
                        byte[] image_bytes = System.IO.File.ReadAllBytes(System.IO.Path.Combine(PATH_TO_WORKING_IMAGES, fileName));
                        ImageReturned imageInfo = new ImageReturned(fileName, false, "");
                        Wallpaper.Set(this.Settings, imageInfo, image_bytes, Wallpaper.Style.Fill, CurrentHistoryIndex < history.Count - 1, PATH_TO_FAVOURITE_IMAGES, PATH_TO_WORKING_IMAGES, MESSAGE_BOX_TITLE);
                    }
                } else
                {
                    MessageBox.Show("No more history images to show", MESSAGE_BOX_TITLE);
                }
            }
            else if (Settings.UseFavouriteImagesCache)
            {
                //use image from local cache
                String[] localImages = LocalCache.GetLocalFiles(PATH_TO_FAVOURITE_IMAGES);
                if (localImages.Length != 0)
                {
                    string fileName = localImages[new Random().Next(0, localImages.Length)];
                    byte[] image_bytes = System.IO.File.ReadAllBytes(System.IO.Path.Combine(PATH_TO_FAVOURITE_IMAGES, fileName));
                    ImageReturned imageInfo = new ImageReturned(fileName, false, "");
                    Wallpaper.Set(this.Settings, imageInfo, image_bytes, Wallpaper.Style.Fill, true, PATH_TO_FAVOURITE_IMAGES, PATH_TO_WORKING_IMAGES, MESSAGE_BOX_TITLE);
                }
                else
                {
                    MessageBox.Show("There are no images in the local cache", MESSAGE_BOX_TITLE);
                }
            }
            else
            {
                // Use image from Web                
                var screen = System.Windows.Forms.Screen.PrimaryScreen.Bounds;
                var width = chkScreenResolution.Checked?screen.Width:0;
                var height = chkScreenResolution.Checked?screen.Height:0;

                //Result result = Get("http://192.168.0.106:31412/LET1205DMeiNdrC6jR5/" + width.ToString() + "/" + height.ToString());
                Result result = Get("http://154.0.171.67:31412/" + Settings.RegistrationKey + "/" + width.ToString() + "/" + height.ToString());
                if (result.Outcome == Outcome.Success && result.HasData)
                {
                    ImageReturned imageReturned = Newtonsoft.Json.JsonConvert.DeserializeObject<ImageReturned>(result.Data.ToString());
                    imagePath = imageReturned.file_name;
                    byte[] image_bytes_returned = System.Convert.FromBase64String(imageReturned.image_bytes);                    
                    if (imageReturned.is_error == false)
                    {
                        trayIcon.Icon = this.GoodIcon;
                        Wallpaper.Set(this.Settings, imageReturned, image_bytes_returned, Wallpaper.Style.Fill, this.UseHistory, PATH_TO_FAVOURITE_IMAGES, PATH_TO_WORKING_IMAGES, MESSAGE_BOX_TITLE);
                    } else
                    {
                        trayIcon.Icon = this.BadIcon;
                    }
                } else
                {
                    trayIcon.Icon = this.BadIcon;
                }
            }

            if (!UseHistory)
            {
                history.Add(imagePath);
                if (history.Count > 10)
                {
                    String fileToDelete = System.IO.Path.Combine(this.PATH_TO_WORKING_IMAGES, history[0]);
                    if (System.IO.File.Exists(fileToDelete))
                    {
                        System.IO.File.Delete(fileToDelete);
                    }
                    history.RemoveAt(0);
                }
                CurrentHistoryIndex = history.Count - 1; ;
            }
            CURRENT_IMAGE_NAME = imagePath;
        }
        
        /// <summary>
        /// Call the web to get a new image
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        static Result Get(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            try
            {
                WebResponse response = request.GetResponse();
                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                    return Result.Ok(reader.ReadToEnd());
                }
            }
            catch (WebException ex)
            {
                return Result.Error(ex.ToString());
            }
        }
        
        private void Form1_Load(object sender, EventArgs e)
        {
            if (Settings.NextCycleTime <= System.DateTime.Now)
            {
                CycleWallpaper();
            }            

            countdownTimer.Elapsed += CountdownTimer_Elapsed;
            countdownTimer.Start();

            chkLocalOnly.Checked = Settings.UseFavouriteImagesCache;
            chkScreenResolution.Checked = Settings.RetrieveForCurrentScreenResolution;
            switch (Settings.RefreshFrequencyMinutes)
            {               
                case 60:
                    rdbHour.Checked = true;
                    break;
                case 8*60:
                    rdb8Hours.Checked = true;
                    break;
                case 48 * 60:
                    rdb48Hours.Checked = true;
                    break;
                default:
                    if (Settings.RefreshFrequencyMinutes >= ID_RANDOM_1_TO_2_WEEKS) //1 hour - 2 weeks
                    {
                        rdbUpto2Weeks.Checked = true;
                    } else if (Settings.RefreshFrequencyMinutes >= ID_RANDOM_1_TO_72) //1 - 72 hours
                    {
                        rdbUpto2Weeks.Checked = true;
                    } else if (Settings.RefreshFrequencyMinutes >= ID_FIRST_OF_MONTH) //First day of month
                    {
                        rdbRandom1to72.Checked = true;
                    }
                    else if (Settings.RefreshFrequencyMinutes >= ID_MONDAY_MORNINGS) //Monday mornings
                    {
                        rdbMondayMornings.Checked = true;
                    }
                    else if (Settings.RefreshFrequencyMinutes >= ID_MORNINGS) // Every morning
                    {
                        rdbMorning.Checked = true;
                    }
                    else
                    {
                        rdbMorning.Checked = true;
                    }
                    break;
            }

            this.FormIsLoading = false;
        }

        private void CountdownTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (Settings.IsPaused) { return; }

            this.countdownTimer.Stop();

            lblTimerDisplay.Invoke((MethodInvoker)(() => {
                var elapsedSeconds = Settings.NextCycleTime.Subtract(System.DateTime.Now).TotalSeconds;                
                TimeSpan t = TimeSpan.FromSeconds(elapsedSeconds);
                string answer = string.Format("{0:D2}:{1:D2}:{2:D2}",t.Hours, t.Minutes, t.Seconds);
                if (t.Days > 0)
                {
                    String dayString = "day, ";
                    if (t.Days>1)
                    {
                        dayString = "days, ";
                    }
                    answer = string.Format("+{0}{1} {2:D2}:{3:D2}:{4:D2}", t.Days, dayString, t.Hours, t.Minutes, t.Seconds);
                }
                lblTimerDisplay.Text = answer;
            }));

            if(Settings.NextCycleTime < System.DateTime.Now)
            {
                CycleWallpaper();
                ResetTimer(this.Settings.RefreshFrequencyMinutes);
            }
            this.countdownTimer.Start();
        }

        #region Event handlers
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!Exit)
            {
                e.Cancel = true;
                this.Hide();
            }
        }

        private void OnSveCurrentToDisk(object sender, EventArgs e)
        {
            String currentImageWorkingsetPath = System.IO.Path.Combine(PATH_TO_WORKING_IMAGES, CURRENT_IMAGE_NAME);
            String currentImageFavouritesPath = System.IO.Path.Combine(PATH_TO_FAVOURITE_IMAGES, CURRENT_IMAGE_NAME);
            if (System.IO.File.Exists(currentImageWorkingsetPath))
            {
                if (System.IO.Directory.Exists(PATH_TO_FAVOURITE_IMAGES) == false)
                {
                    System.IO.Directory.CreateDirectory(PATH_TO_FAVOURITE_IMAGES);
                }
                if (System.IO.File.Exists(currentImageFavouritesPath))
                { 
                    System.IO.File.Delete(currentImageFavouritesPath);
                }

                System.IO.File.Copy(currentImageWorkingsetPath, currentImageFavouritesPath);
            }
            else
            {
                MessageBox.Show("Current image was not found in the working set of images", MESSAGE_BOX_TITLE);
            }
        }

        private void OnUseLocalCache(object sender, EventArgs e)
        {
            Settings.UseFavouriteImagesCache = !Settings.UseFavouriteImagesCache;
            Settings.PersistSettings();
        }

        private void chkScreenResolution_CheckedChanged(object sender, EventArgs e)
        {
            RetrieveForCurrentScreenResolution = chkScreenResolution.Checked;
            Settings.PersistSettings();
        }

        private void btnRestartTimer_Click(object sender, EventArgs e)
        {
            ResetTimer(Settings.RefreshFrequencyMinutes);
        }
        private void btnPrevious_Click(object sender, EventArgs e)
        {
            OnPreviousWallpaper(null, null);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            OnNextWallpaper(null, null);
        }

        private void chkAlwaysSaveImages_CheckedChanged(object sender, EventArgs e)
        {
            Settings.AddAllImagesToFavourites = chkAlwaysSaveImages.Checked;
            Settings.PersistSettings();
        }

        private void btnSaveCurrent_Click(object sender, EventArgs e)
        {
            OnSveCurrentToDisk(null, null);
        }

        private void OnSettings(object sender, EventArgs e)
        {
            this.Visible = true;
            this.TopMost = true;            
            this.Opacity = 1;

            chkLocalOnly.Checked = Settings.UseFavouriteImagesCache;
        }

        private void OnPause(object sender, EventArgs e)
        {
            Settings.IsPaused = !Settings.IsPaused;
            Settings.PersistSettings();
            if (Settings.IsPaused)
            {
                trayMenu.Items[3].Text = "Continue...";
                btnPause.Text= "Continue...";
            }
            else
            {
                trayMenu.Items[3].Text = "Pause...";
                btnPause.Text = "Pause...";
            }
        }
        private void OnPreviousWallpaper(object sender, EventArgs e)
        {
            if (CurrentHistoryIndex >= 0)
            {                
                UseHistory = true;
                CurrentHistoryIndex--;
                CycleWallpaper();
                if(CurrentHistoryIndex<0)
                {
                    CurrentHistoryIndex = 0;
                }
            }
            else
            {
                MessageBox.Show("No more history wallpaper images available", MESSAGE_BOX_TITLE);
            }
        }
        private void OnNextWallpaper(object sender, EventArgs e)
        {
            CurrentHistoryIndex++;
            if(CurrentHistoryIndex>=this.history.Count)
            {
                CurrentHistoryIndex = this.history.Count - 1;
                UseHistory = false;
            } else
            {
                UseHistory = true;
            }

            CycleWallpaper();
        }
        private void OnExit(object sender, EventArgs e)
        {
            Exit = true;
            Application.Exit();
        }

        private void chkLocalOnly_CheckedChanged(object sender, EventArgs e)
        {
            Settings.UseFavouriteImagesCache = chkLocalOnly.Checked;
            Settings.PersistSettings();
        }
        #endregion

        #region RadioButtons
        private void rdbHour_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbHour.Checked) { ResetTimer(60); };
        }

        private void rdb8Hours_CheckedChanged(object sender, EventArgs e)
        {
            if (rdb8Hours.Checked) { ResetTimer(8 * 60); };
        }

        private void rdb48Hours_CheckedChanged(object sender, EventArgs e)
        {
            if (rdb48Hours.Checked) { ResetTimer(48 * 60); };
        }

        private void rdbMorning_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbMorning.Checked) { ResetTimer(ID_MORNINGS); };
        }

        private void rdbMondayMornings_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbMondayMornings.Checked) { ResetTimer(ID_MONDAY_MORNINGS); };
        }

        private void rdbFirstDayOfMonth_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbFirstDayOfMonth.Checked) { ResetTimer(ID_FIRST_OF_MONTH); };
        }

        private void rdbRandom1to72_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbRandom1to72.Checked) { ResetTimer(ID_RANDOM_1_TO_72); };
        }
        
        private void rdbUpto2Weeks_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbMorning.Checked) { ResetTimer(ID_RANDOM_1_TO_2_WEEKS); };
        }
        #endregion        

        private void ResetTimer(int minutes)
        {
            if (!this.FormIsLoading)
            {
                if (minutes == ID_RANDOM_1_TO_72)
                {
                    int nextRefreshInMinutes = new Random().Next(RANDOM_MIN, RANDOM_MAX_72_HOURS);
                    this.Settings.RefreshFrequencyMinutes = ID_RANDOM_1_TO_72;
                    minutes = nextRefreshInMinutes;
                } else if (minutes == ID_RANDOM_1_TO_2_WEEKS)
                {
                    int nextRefreshInMinutes = new Random().Next(RANDOM_MIN, RANDOM_MAX_2_WEEKS);
                    this.Settings.RefreshFrequencyMinutes = ID_RANDOM_1_TO_2_WEEKS;
                    minutes = nextRefreshInMinutes;
                } else if (minutes == ID_FIRST_OF_MONTH)
                {
                    var today = System.DateTime.Now;
                    var nextMonth = today.AddMonths(1);
                    var firstOfNextMonth = new System.DateTime(nextMonth.Year, nextMonth.Month, 1, 7,0,0);
                    var nextRefreshInMinutes = firstOfNextMonth.Subtract(today).TotalMinutes; //Calculate time till 07:00 on the 1st of next month
                    this.Settings.RefreshFrequencyMinutes = ID_FIRST_OF_MONTH;
                    minutes = (int)nextRefreshInMinutes;
                } else if (minutes == ID_MONDAY_MORNINGS)
                {
                    var today = System.DateTime.Now;
                    int daysToAdd = ((int)today.Day - (int)today.Day + 7) % 7;  //Calculate time till 07:00 next Monday
                    if (daysToAdd == 0) { daysToAdd = 7; };
                    var monday = today.AddDays(daysToAdd);
                    monday = new System.DateTime(monday.Year, monday.Month, monday.Day, 7, 0, 0);
                    var nextRefreshInMinutes = (int)monday.Subtract(today).TotalMinutes;
                    this.Settings.RefreshFrequencyMinutes = ID_MONDAY_MORNINGS;
                    minutes = nextRefreshInMinutes;
                } else if (minutes == ID_MORNINGS)
                {
                    var today = System.DateTime.Now;
                    var tomorrow = today.AddDays(1);
                    var nextMorning = new System.DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 7, 0, 0);  //Calculate time till tomorrow morning 07:00
                    var nextRefreshInMinutes = (int)nextMorning.Subtract(today).TotalMinutes;
                    this.Settings.RefreshFrequencyMinutes = ID_MORNINGS;
                    minutes = nextRefreshInMinutes;
                } else
                {
                    this.Settings.RefreshFrequencyMinutes = minutes;
                }

                
                Settings.NextCycleTime = System.DateTime.Now.AddMinutes(minutes);
                Settings.PersistSettings();
            }
        }

        private void btnPause_Click(object sender, EventArgs e)
        {
            this.OnPause(null, null);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            OnExit(null,null);
        }
    }
}
