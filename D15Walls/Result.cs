﻿using System;
using System.Collections.Generic;
using System.Text;

namespace D15Walls
{
    /// <summary>
    /// An outcome of some operation
    /// </summary>
    public enum Outcome
    {
        Success = 1,
        Fail = 0
    }
    /// <summary>
    /// Indicates the success of a process and contains data to be returned if available
    /// </summary>
    /// <typeparam name="T">The type of data to be return if Result indicates a success</typeparam>
    public struct Result
    {
        public Outcome Outcome { get; private set; }
        public bool HasData { get; private set; }
        public object Data { get; private set; }

        /// <summary>
        /// Create a new Result object
        /// </summary>
        /// <param name="outcome"></param>
        /// <param name="hasData"></param>
        /// <param name="data"></param>
        private Result(Outcome outcome, bool hasData, object data)
        {
            this.Outcome = outcome;
            this.HasData = hasData;
            this.Data = data;
        }

        /// <summary>
        /// Result is ok and contains no data
        /// </summary>
        /// <returns></returns>
        public static Result Ok()
        {
            return new Result(Outcome.Success, false, "");
        }
        /// <summary>
        /// Result is ok and contains the expected data and data type
        /// </summary>
        /// <param name="data">Data to return to the caller</param>
        /// <returns></returns>
        public static Result Ok(object data)
        {
            return new Result(Outcome.Success, true, data);
        }
        /// <summary>
        /// Result indicates a failure and contains an error description
        /// </summary>
        /// <param name="errorDescription">A description of the error</param>
        /// <returns></returns>
        public static Result Error(string errorDescription)
        {
            return new Result(Outcome.Fail, true, errorDescription);
        }
        /// <summary>
        /// Result indicates a failure and contains data in an object
        /// </summary>
        /// <param name="data">Data to return to caller</param>
        /// <returns></returns>
        public static Result Error(object data)
        {
            return new Result(Outcome.Fail, true, data);
        }

        public override string ToString()
        {
            string serialised = this.Outcome.ToString() + ": ";
            if (this.HasData)
            {
                serialised = serialised + "Data > ";
                if (this.Data != null)
                {
                    serialised = serialised + this.Data.ToString();
                }
                else
                {
                    serialised = serialised + "NULL";
                }
            }
            else
            {
                serialised = serialised + "No additional data";
            }

            return serialised;
        }
    }
}
