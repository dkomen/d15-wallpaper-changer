﻿using Microsoft.Win32;
using System;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;


namespace D15Walls
{
    class Wallpaper
    {
        const int SPI_SETDESKWALLPAPER = 20;
        const int SPIF_UPDATEINIFILE = 0x01;
        const int SPIF_SENDWININICHANGE = 0x02;

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        static extern int SystemParametersInfo(int uAction, int uParam, string lpvParam, int fuWinIni);

        public enum Style : int
        {
            Fill,
            Fit,
            Span,
            Stretch,
            Tile,
            Center
        }

        public Wallpaper() { }

        public static void Set(Settings settings, ImageReturned imageReturned, byte[] image_bytes_returned, Style style, bool fromCache, string imagesFavouritesPath, string imagesWorkingsetPath, string messageBoxTitle)
        {
            string savePath = System.IO.Path.Combine(imagesWorkingsetPath, imageReturned.file_name);
            string tempPath = Path.Combine(Path.GetTempPath(), "wallpaper.jpeg");
            if (!fromCache)
            {
                System.IO.Stream s = new System.IO.MemoryStream(image_bytes_returned);
                System.Drawing.Image img = System.Drawing.Image.FromStream(s);
                img.Save(tempPath, System.Drawing.Imaging.ImageFormat.Jpeg);
                SaveImage(imagesWorkingsetPath, imageReturned.file_name, img);
                if (settings.AddAllImagesToFavourites)
                {
                    SaveImage(imagesFavouritesPath, imageReturned.file_name, img);
                }
                img.Dispose();
            }
            else
            {
                if (System.IO.File.Exists(savePath))
                {
                    System.Drawing.Image img = System.Drawing.Image.FromFile(savePath);
                    img.Save(tempPath, System.Drawing.Imaging.ImageFormat.Jpeg);
                    img.Dispose();
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show("History image file was not found in cache", messageBoxTitle);
                }
            }

            RegistryKey key = Registry.CurrentUser.OpenSubKey(@"Control Panel\Desktop", true);
            if (style == Style.Fill)
            {
                key.SetValue(@"WallpaperStyle", 10.ToString());
                key.SetValue(@"TileWallpaper", 0.ToString());
            }
            if (style == Style.Fit)
            {
                key.SetValue(@"WallpaperStyle", 6.ToString());
                key.SetValue(@"TileWallpaper", 0.ToString());
            }
            if (style == Style.Span) // Windows 8 or newer only!
            {
                key.SetValue(@"WallpaperStyle", 22.ToString());
                key.SetValue(@"TileWallpaper", 0.ToString());
            }
            if (style == Style.Stretch)
            {
                key.SetValue(@"WallpaperStyle", 2.ToString());
                key.SetValue(@"TileWallpaper", 0.ToString());
            }
            if (style == Style.Tile)
            {
                key.SetValue(@"WallpaperStyle", 0.ToString());
                key.SetValue(@"TileWallpaper", 1.ToString());
            }
            if (style == Style.Center)
            {
                key.SetValue(@"WallpaperStyle", 0.ToString());
                key.SetValue(@"TileWallpaper", 0.ToString());
            }

            SystemParametersInfo(SPI_SETDESKWALLPAPER,
                0,
                tempPath,
                SPIF_UPDATEINIFILE | SPIF_SENDWININICHANGE);
        }

        static void SaveImage(string directoryPath, string imageFileName, System.Drawing.Image img)
        { 
            if (System.IO.Directory.Exists(directoryPath) == false)
            {
                System.IO.Directory.CreateDirectory(directoryPath);
            }

            String fullPath = System.IO.Path.Combine(directoryPath, imageFileName);
            if (System.IO.File.Exists(fullPath))
            {
                System.IO.File.Delete(fullPath);
            }
            img.Save(fullPath, System.Drawing.Imaging.ImageFormat.Jpeg);
        }
    }
}
