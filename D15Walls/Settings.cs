﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json.Serialization;

namespace D15Walls
{
    class Settings
    {
        //public string[] History { get; set; }
        public bool UseFavouriteImagesCache { get; set; }
        public DateTime NextCycleTime { get; set; }
        public bool IsPaused { get; set; }
        public bool AddAllImagesToFavourites { get; set; }
        public int RefreshFrequencyMinutes { get; set; }
        public bool RetrieveForCurrentScreenResolution { get; set; }
        public string RegistrationKey { get; set; }

        private string _settingsPath = String.Empty;

        public Settings() { }
        public Settings(string settingsPath, string messageBoxTitle, int defaultRefreshFrequency)
        {
            _settingsPath = System.IO.Path.Combine(settingsPath, "settings.json");
            if (System.IO.File.Exists(_settingsPath))
            {
                try
                {
                    string serialisedSettings = System.IO.File.ReadAllText(_settingsPath);
                    Settings settings = Newtonsoft.Json.JsonConvert.DeserializeObject<Settings>(serialisedSettings);
                    //this.History = settings.History;
                    this.IsPaused = settings.IsPaused;
                    this.RefreshFrequencyMinutes = settings.RefreshFrequencyMinutes;
                    this.NextCycleTime = settings.NextCycleTime;
                    this.UseFavouriteImagesCache = settings.UseFavouriteImagesCache;
                    this.RetrieveForCurrentScreenResolution = settings.RetrieveForCurrentScreenResolution;
                    this.AddAllImagesToFavourites = settings.AddAllImagesToFavourites;
                    if (String.IsNullOrEmpty(settings.RegistrationKey))
                    {
                        var registrationinputBoxForm = new InputBox(messageBoxTitle);
                        if (registrationinputBoxForm.RegistrationKey != string.Empty)
                        {
                            this.RegistrationKey = registrationinputBoxForm.RegistrationKey;
                        }
                    } else
                    {
                        this.RegistrationKey = settings.RegistrationKey;
                    }
                }
                catch (Exception ex)
                {
                    ex = ex;
                }
            } else
            {
                this.IsPaused = false;
                this.RefreshFrequencyMinutes = defaultRefreshFrequency;
                this.NextCycleTime = System.DateTime.Now;
                this.UseFavouriteImagesCache = false;
                this.RetrieveForCurrentScreenResolution = true;
                this.AddAllImagesToFavourites = false;

                var registrationinputBoxForm = new InputBox(messageBoxTitle);
                if (registrationinputBoxForm.RegistrationKey != string.Empty)
                {
                    this.RegistrationKey = registrationinputBoxForm.RegistrationKey;
                }                
            }
        }

        public void PersistSettings()
        {
            if(System.IO.File.Exists(_settingsPath))
            {
                System.IO.File.Delete(_settingsPath);
            }

            string serialised = Newtonsoft.Json.JsonConvert.SerializeObject(this);

            System.IO.File.WriteAllText(_settingsPath, serialised);
        }
    }
}
