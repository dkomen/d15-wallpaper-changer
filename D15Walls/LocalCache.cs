﻿using System;
using System.Collections.Generic;
using System.Text;

namespace D15Walls
{
    class LocalCache
    {
        /// <summary>
        /// Get all the files in the specified directory
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static String[] GetLocalFiles(string path)
        {
            return System.IO.Directory.GetFiles(path);
        }
    }
}
