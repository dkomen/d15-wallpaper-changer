﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace D15Walls
{
    public partial class InputBox : Form
    {
        public String RegistrationKey { get; set; }
        public InputBox(string messageBoxTitle)
        {
            InitializeComponent();
            this.Text = messageBoxTitle;
            this.AcceptButton = btnOk;
            this.CancelButton = btnCancel;
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.StartPosition = FormStartPosition.CenterScreen;
            this.ShowInTaskbar = false;
            this.ShowDialog();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.RegistrationKey = txtRegistrationKey.Text;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.RegistrationKey = string.Empty;
            this.Close();
        }
    }
}
