﻿using System;
using System.Collections.Generic;
using System.Text;

namespace D15Walls
{
    class ImageReturned
    {
        public String file_name {get; set;}
        public String image_bytes { get; set; }
        public bool is_error { get; set; }
        public String message { get; set; }
        public int images_found { get; set; }

        public ImageReturned(String fileName, bool isError, String message)
        {
            this.file_name = fileName;
            this.is_error = isError;
            this.message = message;
        }
    }
}
